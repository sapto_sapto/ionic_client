import { RegisterResultPage } from './../pages/register-result/register-result';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { RegisterPage } from '../pages/register/register';
import { RegisterPinPage } from '../pages/register-pin/register-pin';
import { RegisterPageModule } from '../pages/register/register.module';
import { RegisterConfirmPage } from '../pages/register-confirm/register-confirm';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    RegisterPage,
    RegisterPinPage,
    RegisterConfirmPage,
    RegisterResultPage
    ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    RegisterPage,
    RegisterPinPage,
    RegisterConfirmPage,
    RegisterResultPage
    ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
