import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterResultPage } from './register-result';

@NgModule({
  declarations: [
    RegisterResultPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterResultPage),
  ],
})
export class RegisterResultPageModule {}
