import { NgModule } from '@angular/core';
import { IonicPageModule,NavController, NavParams } from 'ionic-angular';
import { RegisterPinPage } from './register-pin';


@NgModule({
  declarations: [
    RegisterPinPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPinPage),
  ],
})
export class RegisterPinPageModule {


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }
}
