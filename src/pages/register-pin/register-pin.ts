import { RegisterConfirmPage } from './../register-confirm/register-confirm';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterPinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-pin',
  templateUrl: 'register-pin.html',
})
export class RegisterPinPage {
  registerConfirmPage = RegisterConfirmPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPinPage');
  }

}
